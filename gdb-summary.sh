


##### basic commands
(GDB) run
(GDB) break <line>
(GDB) break <class>:::<function>
(GDB) break <file>.cpp:<line> #breakpoint in another file
(GDB) continue
(GDB) next
(GDB) step
(GDB) finish # step out from function
(GDB) frame     #show frames
(GDB) frame <frame-number>    # switch to frame frames
(GDB) backtrace   # backtrace
(GDB) ptype   # show variable type

###### examine memory
(GDB) x/nfu addr
n, the repeat count
f, the display format('x', 'd', 'u', 'o', 't', 'a', 'c', 'f', 's'),
u, the unit size (b Bytes) (h Halfwords) (w Words) (four bytes).(g Giant)

##### example to automate gdb
nano init.gdb
    set environement LD_PRELOAD=./mylib.so
    set logging file gdb.log
    set logging enabled
gdb ./out -x init.gdb   # or create ~/.gdbinit

##### go to previous line
(GDB) record
(GDB) reverse-next
(GDB) reverse-step

##### display variable every next
(GDB) display var_name
(GDB) undisplay var_name  # remove display variable

##### conditional
(GDB) b 32 if (idx > 10)
(GDB) watch <var_name> if (<var_name> > 10)


##### debuging with threads
(GDB) info threads
(GDB) thread <num_thread>