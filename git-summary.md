## git config
    configuration file:
    /etc/gitconfig
    ~/.gitconfig
    <project_dir>/.git/config

    $ git config --global user.name "chlayfa.anouar"  # [--global | --system]
    $ git config --global user.email "chlayfa.anouar@gmail.com"
    $ git config --unset user.email
    $ git config --get user.email
    $ git config user.name #(show user.name)
    $ git config --list #show config list
    $ git config --get remote.origin.url 

## show, status, log, diff, blame
    $ git status
    $ git diff  # (between WD and index)
    $ git diff HEAD # (between WD and repo)
    $ git diff --cached # (index WD and repo)
    $ git diff --staged # (index WD and repo)
    $ git log -3  #last 3 commits, (HEAD~) after last, (HEAD^) parent
    $ git log -p file.c # show history for file or directory
    $ git show --graph --decorate --oneline # show graph
    $ git log --all --graph --pretty=oneline --abbrev-commit # show branches tree
    $ git blame -L 5,9 file.c   # show who edit file

## remove, nename
    $ git rm --cached file.c   # (stage a file for removal)
    $ git mv file.c file.cpp   # (rename a file METH1)
    $ git mv file.c file.cpp   # (rename a file METH2)\
    $ git add file.c file.cpp   #

## branching
    $ git branch    # show branches
    $ git branch my-branch  # create branch
    $ git checkout my-branch    # switch to branch
    $ git switch my-branch    # switch to branch
    $ git checkout-b my-branch    # create and switch branch
    $ git branch -d <branch>    # delete local branch (-D : force )
    $ git push origin  --delete siwar-branch # delete branch  remotly
    $ git merge other-branch
    $ git rebase other-branch

## stash
    $ git stash --include-untracked
    $ git stash push -m "stash message" # stash with message
    $ git stash apply
    $ git stash apply stash@{n} # n index of stash
    $ git stash drop            # drop top hash, stash@{0}
    $ git stash drop stash@{n}  # drop specific stash - see git stash list
    $ git stash clear
    $ git stash list

## revert
    $ git revert <some-commit>

## git reset
    $ git reset --mixed HEAD    # reset repo and index (default reset)
    $ git reset --hard <some-commit>    # reset repo, index and WD
    $ git reset --soft <some-commit>    # reset only repo

## add, commit
    $ git add <file or folder>
    $ git commit -m <some-msg>
    $ git commit --amend # change comment of the last commit
    $ git commit -am <some-msg>     # commit all tracked files with msg

## remote
    $ git remote add origin <remote-url>  # add a new remote
    $ git remote set-url origin <remote-url>    # change remote url
## synchronize
    $ git fetch
    $ git pull
    $ git pull --rebase
    $ git push
    $ git diff origin/master # show fetched changes from origin/<branch>
    $ git log origin/master # show fetched commits from origin/<branch>


